# Docker for Tensorflow + Nuxt + Flask + Redis + RQ

This repo is a M.W.E. of a common development paradigm I find myself in, namely
having to develop:

1. a library / module to support a TensorFlow model,
2. a TensorFlow model which can be imported,
3. the same model for use with `tf-serving`,
4. an api to feature / make available the trained model, and
5. a user friendly front end to utilize the "backend".

In this repository you will find:

1. a custom python library `pyapp`,
2. a simple task-based flask app (`backend`),
3. a nuxt app (`frontend`),
4. a notebook for making toy model (`notebooks`),
5. the saved models (`models`)

As this is a _minimal_ example, here `pyapp` does very little. It simply sets
the flask app's name. The full interconnectivity of the app is as follows.

The nuxt frontend (built with vue, vuetify, axios, etc) makes requests to
the flask backend. The request structure is two fold. First is to enqueue the task,
which is handled via a redis server and rq, and the second is to check the status of the
task until complete. Normally the task in question in some function or wrapper
over a series of functions from `pyapp`. In this case it is the "trained" toy model.
In the case of the of `tf-serving` model, the enqueued task is a request to severed model.



**DISCLAIMER**: I am not a docker guru. Utilizes this at your own peril.

If I did this right, in theory:

```bash
# for training the model
docker-compose -f docker-compose.ai.development.yml build
docker-compose -f docker-compose.ai.development.yml up

# for developing the web app
docker-compose -f docker-compose.web.production.yml -f docker-compose.web.development.yml build
docker-compose -f docker-compose.web.production.yml -f docker-compose.web.development.yml up


# for serving the model as a web app
docker-compose -f docker-compose.web.production.yml -f build
docker-compose -f docker-compose.web.production.yml -f up
```

should work

if this is too much, there is a small python script that can be called instead:

```bash
# python docker.py -w {ai|web} (-p) -c {build|up|down}

# spin up the web containers in development mode
python docker.py -w web -c up
```

## ENV variables
For clarity, the `.env` file is a part of this git repository. At the moment, the following
environment variables are used:

1. `API_VERSION`: the version of the "backend" api (different from `tf-serving`'s versioning). Defaults to `0`. Used in:
    - `/backend/api/views.py`
    - `/frontend/plugins/axios.js`

2. `REDIS_URL`: the location of the redis server. Defaults to `redis://redis:6379`. Used in:
    - `/backend/app/settings.py`
    - `/backend/manage.py`

3. `SECRET_KEY`: utilized for the flask app and the express app (nuxt server middleware) Defaults to `!_@M_W3@K_S3cr3T` Used in:
    - `/backend/app/settings.py`
    - `/frontend/nuxt.config.js`

4. `SINGLE_USERNAME`: a single user for light page restrictions on the frontend. Defaults to `demo`. Used in:
    - `/frontend/restricted_pages/index.js`

5. `SINGLE_PASSWORD`: see above. Defaults to `test`
    - `/frontend/restricted_pages/index.js`

6. `PUBLIC_API_URL`: the location of the flask api. Defaults to `http://localhost:9061`. Used in:
    - `/frontend/plugins/axios.js`

7. `HOST`: where nuxt is hosted. Defaults to `0.0.0.0`.

8. `PYTHONPATH`: where flask should look for python variables (required for unpublished modules e.g. `pyapp`).




## TF Serving

A served model from `tf-serving` is also provided. There seems to be breaking changes with tf 2.0, so the model is exported via 1.15

To run the image without `docker-compose`
```bash
docker run --rm -p 8501:8501 \
  --mount type=bind,source=$(pwd),target=$(pwd) \
  -e MODEL_BASE_PATH=$(pwd)/models/serving \
  -e MODEL_NAME=toy -t tensorflow/serving:latest
```

### CURL model
```
curl http://localhost:8501/v1/models/toy
```
should yield:

```json
{
 "model_version_status": [
  {
   "version": "1",
   "state": "AVAILABLE",
   "status": {
    "error_code": "OK",
    "error_message": ""
   }
  }
 ]
}
```

### POST a request
```
curl -d '{"instances": [[[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],  [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]]]}' -X POST http://localhost:8501/v1/models/toy:predict
```

should yield:

```json
{
    "predictions": [[0.105554141, 0.0696139038, 0.192474931, 0.122520126, 0.126098737, 0.0742906258, 0.119571023, 0.065496929, 0.0684709623, 0.0559086949]
    ]
}
```
