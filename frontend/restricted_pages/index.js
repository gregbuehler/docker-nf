import express from 'express'

const username = process.env.SINGLE_USERNAME || 'demo'
const password = process.env.SINGLE_PASSWORD || 'test'

console.log('inside restriced_pages/index.js', {
  username, password,
  envUsername: process.env.SINGLE_USERNAME,
  envPassword: process.env.SINGLE_PASSWORD
})

// Create express router
const router = express.Router()

// Transform req & res to have the same API as express
// So we can use res.status() & res.json()
const app = express()
router.use((req, res, next) => {
  Object.setPrototypeOf(req, app.request)
  Object.setPrototypeOf(res, app.response)
  req.res = res
  res.req = req
  next()
})

// Add POST - /api/login
router.post('/login', (req, res) => {

  if (req.body.username === username && req.body.password === password) {
    req.session.authUser = { username }
    return res.json({ username })
  }
  res.status(401).json({ message: 'Bad credentials' })
})

// Add POST - /api/logout
router.post('/logout', (req, res) => {
  delete req.session.authUser
  res.json({ ok: true })
})

// Export the server middleware
export default {
  path: '/restricted_pages',
  handler: router
}
