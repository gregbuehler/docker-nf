import setuptools
from pyapp import __name__, __version__

with open("README.md", "r") as fh:
    long_description = fh.read()


setuptools.setup(
    name    = __name__,
    version = __version__,
    author  = "Foo Bar",
    author_email = "foo.bar@internet.online",
    description  = "baz buzz",
    long_description = long_description,
    long_description_content_type = "text/markdown",
    url = "https://pypi.org/project/bar/",
    packages = setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3.6",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
